import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Font } from 'expo';
import { Ionicons } from '@expo/vector-icons';
import { Container, Spinner } from 'native-base'

import Setup from './src/boot/Setup'
import { stylesLight } from './src/containers/styles'

export default class App extends Component {
   state = {
      fontLoaded: false
   }

   async componentDidMount() {
      await Font.loadAsync({
         'Roboto': require('native-base/Fonts/Roboto.ttf'),
         'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
         ...Ionicons.font,
      });

      this.setState({ fontLoaded: true })
   }
   
   render() {
      const { fontLoaded } = this.state
      if (fontLoaded) {
         return (
            <Setup />
         )
      }
      return (
         <Container style={stylesLight.centeredContainer}>
            <Spinner color="#007aff"/>
         </Container>
      )
   }
}

