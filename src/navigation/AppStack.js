import { createStackNavigator, createAppContainer } from 'react-navigation'

import searchForm from '../containers/searchForm';
import list from '../containers/list';
import settings from '../containers/settings';

// login stack
const RootStack = createStackNavigator({
   searchForm: { screen: searchForm },
   list: { screen: list },
   settings: { screen: settings }
}, {
   headerMode: 'none',
   initialRouteName: 'searchForm'
})

const AppStack = createAppContainer(RootStack);

export default AppStack;