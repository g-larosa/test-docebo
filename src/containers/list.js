import React, { Component } from 'react'
import { Modal, View, TouchableWithoutFeedback } from 'react-native'
import { Container, Header, Content, List, ListItem, Thumbnail, Text, Left, Body, Right, Button, Icon, Spinner, Radio } from 'native-base'
import { connect } from 'react-redux'

import * as actions from '../store/actions'
import {stylesLight, stylesDark} from './styles';

class list extends Component {
   state = {
      modalVisible: false,
      sortAttr: 'item_price',
      sortDir: ''
   };
   
   isCloseToBottom({ layoutMeasurement, contentOffset, contentSize }) {   
      return layoutMeasurement.height + contentOffset.y 
       >= contentSize.height - 10; 
   }

   setModalVisible( visible ) {
      this.setState({ modalVisible: visible });
   }

   clearAllFilters() {
      this.setState({ 
         sortAttr: '',
         sortDir: '',
         modalVisible: false 
      })
   }

   getCourses (page) {
      let params = {
         itemName: this.props.itemName,
         courseType: this.props.courseType,
         page: page,
         sortAttr: this.state.sortAttr,
         sortDir: this.state.sortDir
      }
      this.props.onGetCourses(params); 
      this.setState({  modalVisible: false })
   }

   render() {
      let styles = this.props.colorScheme === 'dark' ? stylesDark : stylesLight;

      return (
         <Container style={styles.container}>
            <Header style={styles.header}>
               <Left>
                  <Button 
                     transparent
                     onPress={() => this.props.navigation.goBack() } >
                    <Icon style={styles.textColor} name='arrow-back' />
                  </Button>
               </Left>
               <Right>
                  <Button transparent onPress={() => { this.setModalVisible(true); }}>
                     <Text style={styles.textColor} >Filters</Text>
                  </Button>
               </Right>
            </Header>

            <Content onScroll={({ nativeEvent }) => {
               if(!this.props.loading && this.props.hasMore)  {
                  if (this.isCloseToBottom(nativeEvent) ) { 
                     this.getCourses(this.props.currentPage + 1); 
                  }         
               }
            }} > 
            	<Text
                  style={[styles.textColor, {padding: 15 }]}>{this.props.total} Items</Text>
            	<List >
            	{
	            	this.props.items.map((item, i) => {
	            		return (
				            <ListItem thumbnail key={item.item_id+i}>
				               <Left>
				                 <Thumbnail square source={{ uri: 'http:'+item.item_thumbnail }} />
                           </Left>
				               <Body>
				                 <Text style={[styles.textColor, {paddingBottom: 4 }]}>{item.item_name}</Text>
				                 <Text note>{item.course_type} | {item.item_price}</Text>
				                 <Text note numberOfLines={1}>{item.item_description.replace(/<(?:.|\n)*?>/gm, '')}</Text>
                           </Body>
				            </ListItem>
				         )
	            	})
            	}
		         </List>
               {
                  this.props.loading &&
                  <Spinner color={this.props.colorScheme === 'dark' ? '#ffffff' : '#007aff'}/>
               }
               <Modal
                  animationType="slide"
                  transparent={true}
                  visible={this.state.modalVisible} 
                  onRequestClose={()=>{}} >
                  <TouchableWithoutFeedback  onPress={() => this.setState({ modalVisible: false }) } >
                     <View style={styles.backdrop} />
                  </TouchableWithoutFeedback>
                  <View style={styles.modal}>
                     <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between'
                     }}>
                        <Text style={[styles.textColor,{
                           padding: 15,
                           fontWeight: 'bold'
                        }]}>Sort By</Text>
                        <Button onPress={() => this.clearAllFilters() } transparent ><Text>Clear all</Text></Button>
                     </View>
                     <List>
                        <ListItem noBorder style={{paddingTop: 4, paddingBottom:4 }} onPress={() => this.setState({sortDir: 'asc'}) }>
                           <Left>
                             <Text style={styles.textColor}>ASC</Text>
                           </Left>
                           <Right>
                              <Radio 
                                 selected={this.state.sortDir === 'asc'} />
                           </Right>
                        </ListItem>
                        <ListItem noBorder style={{paddingTop: 4, paddingBottom:4 }} onPress={() => this.setState({sortDir: 'desc'}) }>
                           <Left>
                             <Text style={styles.textColor}>DESC</Text>
                           </Left>
                           <Right>
                              <Radio selected={this.state.sortDir === 'desc'} />
                           </Right>
                        </ListItem>
                     </List>
                     <View>
                        <Text style={[styles.textColor, {
                           padding: 15,
                           fontWeight: 'bold'
                        }]}>Filter By</Text>
                     </View>
                     <List>
                     <ListItem noBorder style={{paddingTop: 4, paddingBottom:4 }} onPress={() => this.setState({sortAttr: 'item_price'}) }>
                        <Left>
                          <Text style={styles.textColor}>Price</Text>
                        </Left>
                        <Right>
                           <Radio selected={this.state.sortAttr === 'item_price'} />
                        </Right>
                     </ListItem>
                     </List>
                     <Button disabled={this.state.sortAttr === '' || this.state.sortDir === ''} style={{margin: 15}} block onPress={() => this.getCourses(1)}>
                        <Text>Filter</Text>
                     </Button>
                  </View>
               </Modal>
            </Content>
         </Container>
      )
   }
}

const mapStateToProps = state => {
   return {
      colorScheme: state.theme.colorScheme,
      courseType: state.courses.courseType,
      items: state.courses.items,
      itemName:  state.courses.itemName,
      total: state.courses.total,
      currentPage: state.courses.currentPage,
      hasMore: state.courses.hasMore,
      loading: state.courses.loading
   }
}

const mapDispatchToProps = dispatch => {
   return {
      onGetCourses: (itemName, courseType, page, sortAttr, sortDir) => dispatch(actions.getCourses(itemName, courseType, page, sortAttr, sortDir))
   }
}

export default connect(mapStateToProps, mapDispatchToProps)(list);