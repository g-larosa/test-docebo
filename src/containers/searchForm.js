import React, { Component } from 'react'
import { View } from 'react-native'
import { Container, Header, Content, Form, Item, Button, Input, Text, Picker, Icon, Fab } from 'native-base'
import { connect } from 'react-redux'
import MultiSelect from 'react-native-multiple-select';

import * as actions from '../store/actions'
import {stylesLight, stylesDark} from './styles';

class searchForm extends Component {
   state =  {
      courseType: [],
      itemName: ''
   }

   componentWillMount () {
      this.props.onGetTheme()
   }

   componentWillReceiveProps(nextProps) {
      if(this.props.loading !== nextProps.loading) {
         this.props.navigation.navigate('list');
      }
   }

   onChangeItemName (value) {
      this.setState({
         itemName: value
      })
   }

   onSelectedItemsChange = selectedItems => {
      this.setState({ courseType: selectedItems});
   };

   sendForm = () => {
      let params = {
         itemName: this.state.itemName,
         courseType: this.state.courseType,
         page: 1,
      }
      this.props.onGetCourses(params);
   }

   render() {
      let styles = this.props.colorScheme === 'dark' ? stylesDark : stylesLight;
      const items = [{
          id: 'classroom',
          name: 'Classroom',
        }, {
          id: 'elearning',
          name: 'E-learning',
        }, {
          id: 'mobile',
          name: 'Mobile',
        }, {
          id: 'webinar',
          name: 'Webinar',
        }, {
          id: 'learning_plan',
          name: 'Learning plan',
        }];

      const { courseType } = this.state;

      return (
         <Container style={[styles.container, styles.centeredContainer]}>
            <Fab 
               style={styles.button}
               position="bottomRight"
               onPress={() => this.props.navigation.navigate('settings')} >
               <Icon style={styles.buttonText} name="settings" />
            </Fab>
            <Form >
               <Item>
                  <Input style={styles.input} onChangeText={(value) => this.onChangeItemName(value) } placeholder="Item Name" />
               </Item>
               <View>
                  <MultiSelect
                     styleDropdownMenuSubsection={{marginLeft:15, backgroundColor: this.props.colorScheme === 'dark' ? '#3b3b3d' : '#ffffff'}}
                     styleListContainer={{ backgroundColor: this.props.colorScheme === 'dark' ? '#333333' : '#ffffff'}}
                     styleInputGroup={{ backgroundColor: this.props.colorScheme === 'dark' ? '#333333' : '#ffffff'}}
                     hideTags
                     items={items}
                     fontSize={16}
                     uniqueKey="id"
                     ref={(component) => { this.multiSelect = component }}
                     onSelectedItemsChange={this.onSelectedItemsChange}
                     hideSubmitButton={true}
                     selectedItems={courseType}
                     selectText="Select Type"
                     searchInputPlaceholderText="Search Items..."
                     onChangeInput={ (text)=> console.log(text)}
                     tagRemoveIconColor="#CCC"
                     tagBorderColor="#CCC"
                     tagTextColor="#CCC"
                     selectedItemTextColor="#CCC"
                     selectedItemIconColor="#CCC"
                     itemTextColor={this.props.colorScheme === 'dark' ? '#ffffff' : '#000000'}
                     displayKey="name"
                     searchInputStyle={{ color: '#CCC' }}
                    />
                 <View>
                     {this.multiSelect && this.multiSelect.getSelectedItemsExt(courseType)}
                 </View>
               </View>
               <Button style={styles.button} block onPress={this.sendForm}>
                  <Text style={styles.buttonText}>Search</Text>
               </Button>
            </Form>
         </Container>

      );
   }
}


const mapStateToProps = state => {
   return {
      items: state.courses.items,
      total: state.courses.total,
      loading: state.courses.loading,
      colorScheme: state.theme.colorScheme
   }
}

const mapDispatchToProps = dispatch => {
   return {
      onGetCourses: (params) => dispatch(actions.getCourses(params)),
      onGetTheme: () => dispatch(actions.getTheme())
   }
}

export default connect(mapStateToProps, mapDispatchToProps)(searchForm);