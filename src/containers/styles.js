export const stylesLight = {
   centeredContainer: {
      justifyContent: 'center'
   },
   container: {
      flex: 1
   },
   button: {
      backgroundColor: '#007aff',
      margin: 16
   },
   spinner: {
      color: '#007aff',
   },
   backdrop: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'flex-end',
      alignItems: 'center',
      backgroundColor: 'rgba(0,0,0,0.7)'
   },
   modal: {
      height: 300,
      width: '100%',
      backgroundColor: '#ffffff'
   }
};

export const stylesDark = { 
   header: {
      backgroundColor: '#333333'
   },
   centeredContainer: {
      ...stylesLight.centeredContainer,
   },
   container: {
      ...stylesLight.container,
      backgroundColor: '#3b3b3d'
   },
   input: {
      color: '#ffffff'
   },
   button: {
      ...stylesLight.button,
      backgroundColor: '#cacaca',
   },
   buttonText: {
      color: '#000000'
   },
   textColor: {
      color: '#ffffff'
   },
   backdrop: {
      ...stylesLight.backdrop,
      backgroundColor: 'rgba(255,255,255,0.3)'
   },
   modal: {
      ...stylesLight.modal,
      backgroundColor: '#333333'
   }
};