import React, { Component } from 'react'
import { StyleSheet} from 'react-native'
import { Container, Header, Content, Form, Item, Button, Input, Text, Picker, Icon, Left, Right } from 'native-base'
import { connect } from 'react-redux'

import * as actions from '../store/actions'
import {stylesLight, stylesDark} from './styles'

class settings extends Component {
   state =  {
      colorScheme: this.props.colorScheme,
   }

   onChangecColorScheme(value) {
      this.setState({
         colorScheme: value
      });
   }

   sendForm = () => {
      this.props.onSwitchTheme(this.state.colorScheme);
   }

   render() {
      let styles = this.props.colorScheme === 'dark' ? stylesDark : stylesLight;

      return (
         <Container style={styles.container}>
            <Header style={styles.header}>
               <Left>
                  <Button 
                     transparent
                     onPress={() => this.props.navigation.goBack() } >
                    <Icon style={styles.textColor} name='arrow-back' />
                  </Button>
               </Left>
               <Right></Right>
            </Header>
            <Form>
               <Item  picker last>
                  <Picker
                     mode="dropdown"
                     iosIcon={<Icon style={{color: this.props.colorScheme === 'dark' ? '#ffffff' : '#000000'}}name="arrow-down" />}
                     style={{ width: '100%' }}
                     textStyle={{ color: this.props.colorScheme === 'dark' ? '#ffffff' : '#000000' }}
                     placeholder="Select Theme"
                     placeholderStyle={{ color: "#bfc6ea" }}
                     placeholderIconColor="#007aff"
                     selectedValue={this.state.colorScheme}
                     onValueChange={(value) => this.onChangecColorScheme(value)} >
                        <Picker.Item label="Light" value="light" />
                        <Picker.Item label="Dark" value="dark" />
                  </Picker>
               </Item>
               <Button style={styles.button}  block onPress={this.sendForm}>
                  <Text style={styles.buttonText}>Save</Text>
               </Button>
            </Form>
         </Container>
      );
   }
}


const mapStateToProps = state => {
   return {
      colorScheme: state.theme.colorScheme,
   }
}

const mapDispatchToProps = dispatch => {
   return {
      onSwitchTheme: (colorScheme) => dispatch(actions.switchTheme(colorScheme))
   }
}

export default connect(mapStateToProps, mapDispatchToProps)(settings);