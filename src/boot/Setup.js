import React, { Component } from 'react'
import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'

import coursesReducer from '../store/reducers/courses';
import themeReducer from '../store/reducers/theme';
import AppStack from '../navigation/AppStack'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose


const rootReducer = combineReducers({
  courses: coursesReducer,
  theme: themeReducer
})


const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk))
)

export default class Setup extends Component {
   render() {
      return (
         <Provider store={store}>
            <AppStack />
         </Provider>
      )
   }
}
