import { AsyncStorage } from 'react-native'
import * as actionTypes from '../actions/actionTypes'


export const switchTheme = (colorScheme) => {
	AsyncStorage.setItem('colorScheme', colorScheme);
   return{
      type: actionTypes.SWITCH_THEME,
      colorScheme: colorScheme
   };
};


export const getThemeStart = () => {
   return {
      type: actionTypes.GET_THEME_START
   }
}

export const getThemeSuccess = (colorScheme) => {
   return {
      type: actionTypes.GET_THEME_SUCCESS,
      colorScheme: colorScheme
   }
}

export const getTheme = () => {
	return dispatch => {
		dispatch(getThemeStart())
		AsyncStorage.getItem('colorScheme').then(value => {
			let colorScheme = value ? value : 'light'
			dispatch(getThemeSuccess(colorScheme))
		})
	}
};



