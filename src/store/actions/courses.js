import * as actionTypes from '../actions/actionTypes'

export const coursesStart = () => {
   return {
      type: actionTypes.COURSES_START
   }
}

export const coursesSuccess = (data, courseType, itemName) => {
   return {
      type: actionTypes.COURSES_SUCCESS,
      courseType: courseType,
      itemName: itemName,
      items: data.items,
      currentPage: data.current_page,
      total: data.total_count,
      hasMore: data.has_more_data 
   }
}

export const coursesFail = (error) => {
   return {
      type: actionTypes.COURSES_FAIL,
      error: error
   }
}


export const getCourses = (params) => {
   return dispatch => {
      dispatch(coursesStart())

      let queryString = 'search_text='+params.itemName+'&page'+params.page;
      if(params.courseType.length) {
         params.courseType.map(type => {
            queryString += '&type[]='+type
         })
      }

      if(params.sortAttr && params.sortDir) {
         queryString += '&sort_attr='+params.sortAttr+'&sort_dir='+params.sortDir
      }

      fetch(`https://demomobile.docebosaas.com/learn/v1/catalog?${queryString}`)
         .then((response) => response.json())
         .then((responseJson) => {
            dispatch(coursesSuccess(responseJson.data, params.courseType, params.itemName ))

         })
         .catch((error) => {
            console.error(error);
            dispatch(coursesFail(error.response.data.message))
         });
   }
}