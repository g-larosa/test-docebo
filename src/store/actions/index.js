export {
   getCourses
} from './courses'

export {
   switchTheme,
   getTheme
} from './theme'