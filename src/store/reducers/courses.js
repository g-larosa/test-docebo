import * as actionTypes from '../actions/actionTypes'

const initialState = {
	loading: true,
	error: false,
   items: [],
   currentPage: null,
   total: 0,
   hasMore: false,
   courseType: 'all',
   itemName: ''
}

const reducer = (state = initialState, action) => {
   switch (action.type) {
	 	case actionTypes.COURSES_START:
      	return {
	         ...state,
	         loading: true,
	         error: false
      	}
      case actionTypes.COURSES_SUCCESS:
         let items = action.items;
         if(action.currentPage > 1) {
            items = [...state.items, ...action.items];
         }
      	return {
	         ...state,
	         items: items,
      		currentPage: action.currentPage,
            courseType: action.courseType,
            itemName: action.itemName,
      		total: action.total ? action.total : 0,
            hasMore: action.hasMore,
	         loading: false,
	         error: false,

      	}
      case actionTypes.COURSES_FAIL:
      	return {
	         ...state,
	         loading: false,
	         error: true
      	}
   default:
         return state
   }
}

export default reducer