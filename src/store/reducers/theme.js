import * as actionTypes from '../actions/actionTypes'

const initialState = {
	loading: true,
   colorScheme: 'light'
}

const reducer = (state = initialState, action) => {
   switch (action.type) {
	 	case actionTypes.SWITCH_THEME:
      	return {
	         ...state,
	         colorScheme: action.colorScheme,
	         error: false
      	}
      case actionTypes.GET_THEME_START:
         return {
            ...state,
            loading: true
         }
      case actionTypes.GET_THEME_SUCCESS:
         return {
            ...state,
            colorScheme: action.colorScheme,
            loading: false
         }
   default:
         return state
   }
}

export default reducer